// Copyright 2020 Raptor Engineering, LLC
// Released under the terms of the two-clause BSD license

module test_wishbone_module(
		// Wishbone signals
		input wire wb_cyc,
		input wire wb_stb,
		input wire wb_we,
		input wire [31:0] wb_addr,
		input wire [7:0] wb_dat_w,
		output wire [7:0] wb_dat_r,
		output wire wb_ack,
		output wire wb_err,

                input wire peripheral_reset,
                input wire peripheral_clock
	);

	reg wb_ack_reg = 0;
	assign wb_ack = wb_ack_reg;
	reg [7:0] wb_dat_r_reg = 0;
	assign wb_dat_r = wb_dat_r_reg;

	reg [7:0] wishbone_transfer_state = 0;
	reg wb_data_cycle_type = 0;
	reg [31:0] wb_address_reg = 0;
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			wb_ack_reg <= 0;
			wishbone_transfer_state <= 0;
		end else begin
			case (wishbone_transfer_state)
				0: begin
					if (wb_cyc && wb_stb) begin
						wb_data_cycle_type <= wb_we;
						wb_address_reg <= wb_addr;
						wishbone_transfer_state <= 1;
					end
				end
				1: begin
					wb_dat_r_reg <= (wb_address_reg[7:0] + 8'h80);
					wb_ack_reg <= 1;

					wishbone_transfer_state <= 2;
				end
				2: begin
					wb_ack_reg <= 0;
					wishbone_transfer_state <= 0;
				end
			endcase
		end
	end
endmodule